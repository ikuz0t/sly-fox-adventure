using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public GameObject deathEffect;

    public GameObject collectible;
    [Range(0, 100)] public float chanceToDrop;

    private Rigidbody2D rb => GetComponent<Rigidbody2D>();

    private void Start()
    {
        //ui = GameObject.Find("Canvas").GetComponent<UI>();
    }
    // Update is called once per frame
    void Update() => transform.right = rb.velocity;
   

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Enemy")
        {
            Debug.Log("Hit Enemy");

            Destroy(gameObject);
            Destroy(other.gameObject);

            Instantiate(deathEffect, other.transform.position, other.transform.rotation);

            LevelManager.instance.score += 10;

            UIController.instance.UpdateScore();

            float dropSelect = Random.Range(0, 100f);

            if (dropSelect <= chanceToDrop)
            {
                Instantiate(collectible, other.transform.position, other.transform.rotation);
            }

            string soundStatus = PlayerPrefs.GetString("soundStatus", "true");
            bool isSoundOn = bool.Parse(soundStatus);

            if (isSoundOn)
            {
                AudioManager.instance.PlaySFX(3);
            }
        }


    }
}
