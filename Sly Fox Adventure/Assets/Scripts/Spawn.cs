﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn : MonoBehaviour
{
    public GameObject spawn;

    public float spawnInterval = 1f; // Độ trễ giữa các lần spawn

    private float timer = 0f; // Biến đếm thời gian

    int count = 0;
    void Update()
    {
        timer += Time.deltaTime; // Tính toán thời gian trôi qua

        if (timer >= spawnInterval && count <= 10)
        {
            Instantiate(spawn, transform.position, Quaternion.identity);
            timer = 0f; // Reset lại timer

            count++;
        }
    }
}
