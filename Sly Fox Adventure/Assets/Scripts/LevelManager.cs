﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
	public static LevelManager instance;

	public float waitToRespawn;

	public int gemsCollected;

	public string levelToLoad;

	public float timeInLevel;

	public int score;

	public AudioSource audioSource;

	private void Awake()
	{
		instance = this;
	}

	// Start is called before the first frame update
	void Start()
	{
		timeInLevel = 0f;

        string musicStatus = PlayerPrefs.GetString("musicStatus", "true");
        bool isMusicOn = bool.Parse(musicStatus);

        if (isMusicOn)
        {
            audioSource.Play();
        }
        else
        {
            audioSource.Stop();
        }
    }

	// Update is called once per frame
	void Update()
	{
		timeInLevel += Time.deltaTime;
	}

	public void RespawnPlayer()
	{
		StartCoroutine(RespawnCo());
	}

	private IEnumerator RespawnCo()
	{
        score -= 10;

        UIController.instance.UpdateScore();

        PlayerController.instance.gameObject.SetActive(false);

        string soundStatus = PlayerPrefs.GetString("soundStatus", "true");
        bool isSoundOn = bool.Parse(soundStatus);

        if (isSoundOn)
        {
	        AudioManager.instance.PlaySFX(8);
        }

		yield return new WaitForSeconds(waitToRespawn - (1f / UIController.instance.fadeSpeed));

		UIController.instance.FadeToBlack();

		yield return new WaitForSeconds((1f / UIController.instance.fadeSpeed) + .2f);

		UIController.instance.FadeFromBlack();

        PlayerPrefs.DeleteKey("useGun");
        //PlayerController.instance.useGun = false;

        PlayerController.instance.gameObject.SetActive(true);

		PlayerController.instance.transform.position = CheckpointController.instance.spawnPoint;

		PlayerHealthController.instance.currentHealth = PlayerHealthController.instance.maxHealth;
		UIController.instance.UpdateHealthDisplay();
	}

	public void EndLevel()
	{
		StartCoroutine(EndLevelCo());
	}

	public IEnumerator EndLevelCo()
	{
        string musicStatus = PlayerPrefs.GetString("musicStatus", "true");
        bool isMusicOn = bool.Parse(musicStatus);

        if (isMusicOn)
        {
	        AudioManager.instance.PlayLevelVictory();
        }


        PlayerController.instance.stopInput = true;

		CameraController.instance.stopFollow = true;

		UIController.instance.levelCompleteText.SetActive(true);

		yield return new WaitForSeconds(1.5f);

		UIController.instance.FadeToBlack();

        yield return new WaitForSeconds((1f / UIController.instance.fadeSpeed) + 3f);

		PlayerPrefs.SetInt(SceneManager.GetActiveScene().name + "_unlocked", 1);
		PlayerPrefs.SetString("CurrentLevel", SceneManager.GetActiveScene().name);

		if (PlayerPrefs.HasKey(SceneManager.GetActiveScene().name + "_gems"))
		{
			if (gemsCollected > PlayerPrefs.GetInt(SceneManager.GetActiveScene().name + "_gems"))
			{
				PlayerPrefs.SetInt(SceneManager.GetActiveScene().name + "_gems", gemsCollected);
			}
		}
		else
		{
			PlayerPrefs.SetInt(SceneManager.GetActiveScene().name + "_gems", gemsCollected);
		}

		if (PlayerPrefs.HasKey(SceneManager.GetActiveScene().name + "_time"))
		{
			if (timeInLevel < PlayerPrefs.GetFloat(SceneManager.GetActiveScene().name + "_time"))
			{
				PlayerPrefs.SetFloat(SceneManager.GetActiveScene().name + "_time", timeInLevel);
			}
		}
		else
		{
			PlayerPrefs.SetFloat(SceneManager.GetActiveScene().name + "_time", timeInLevel);
		}

		SceneManager.LoadScene(levelToLoad);
	}
}
