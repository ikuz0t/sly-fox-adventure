﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stompbox : MonoBehaviour
{
    public GameObject deathEffect;

    public GameObject collectible;
    [Range(0, 100)]public float chanceToDrop;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Enemy")
        {
            Debug.Log("Hit Enemy");

            other.transform.parent.gameObject.SetActive(false);

            Instantiate(deathEffect, other.transform.position, other.transform.rotation);

            LevelManager.instance.score += 10;

            UIController.instance.UpdateScore();

            PlayerController.instance.Bounce();


            float dropSelect = Random.Range(0, 100f);

            if(dropSelect <= chanceToDrop)
            {
                Instantiate(collectible, other.transform.position, other.transform.rotation);
            }

            string soundStatus = PlayerPrefs.GetString("soundStatus", "true");
            bool isSoundOn = bool.Parse(soundStatus);

            if (isSoundOn)
            {
                AudioManager.instance.PlaySFX(3);
            }
        }

        
    }
}
