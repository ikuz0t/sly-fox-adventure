﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour
{
    public bool isGem, isHeal, isGun;

    private bool isCollected;

    public GameObject pickupEffect;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag("Player") && !isCollected)
        {
            if(isGem)
            {
                LevelManager.instance.gemsCollected++;
                LevelManager.instance.score += 5;

                isCollected = true;
                Destroy(gameObject);

                Instantiate(pickupEffect, transform.position, transform.rotation);

                UIController.instance.UpdateGemCount();
                UIController.instance.UpdateScore();

                string soundStatus = PlayerPrefs.GetString("soundStatus", "true");
                bool isSoundOn = bool.Parse(soundStatus);

                if (isSoundOn)
                {
                    AudioManager.instance.PlaySFX(6);
                }
            }

            if(isHeal)
            {
                if(PlayerHealthController.instance.currentHealth != PlayerHealthController.instance.maxHealth)
                {
                    PlayerHealthController.instance.HealPlayer();

                    isCollected = true;
                    Destroy(gameObject);

                    Instantiate(pickupEffect, transform.position, transform.rotation);

                    string soundStatus = PlayerPrefs.GetString("soundStatus", "true");
                    bool isSoundOn = bool.Parse(soundStatus);

                    if (isSoundOn)
                    {
                          AudioManager.instance.PlaySFX(7);
                    }
                }
            }

            if(isGun)
            {
                PlayerPrefs.SetString("useGun", "true");
                //useGun = true;

                GunController.instance.ReloadGun();

                Destroy(gameObject);

                Instantiate(pickupEffect, transform.position, transform.rotation);

                string soundStatus = PlayerPrefs.GetString("soundStatus", "true");
                bool isSoundOn = bool.Parse(soundStatus);

                if (isSoundOn)
                {
                    AudioManager.instance.PlaySFX(7);
                }
            }
        }
    }
}
