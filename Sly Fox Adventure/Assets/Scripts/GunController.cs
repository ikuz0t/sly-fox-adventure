using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunController : MonoBehaviour
{
    [SerializeField] private Transform gun;
    [SerializeField] private float gunDistance = 1.5f;

    private bool gunFacing = true;

    [Header("Bullet")]
    [SerializeField] private GameObject bulletPrefab;
    [SerializeField] private float bullerSpeed;
    [SerializeField] private int maxBullet = 15;
    private int currentBullet;

    public static GunController instance;

    private void Start()
    {
        //if (PlayerController.instance.useGun)
        //{
        //    ReloadGun();
        //}
        
        if (PlayerPrefs.HasKey("useGun"))
        {
            ReloadGun();
        }


    }

    private void Awake()
    {
        instance = this;
    }


    // Update is called once per frame
    void Update()
    {
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector3 direction = mousePos - gun.position;

        gun.rotation = Quaternion.Euler(new Vector3(0, 0, Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg));

        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        gun.position = transform.position + Quaternion.Euler(0, 0, angle) * new Vector3(gunDistance, 0, 0);

        if (Input.GetKeyDown(KeyCode.Mouse0) && HaveBullet() && /*PlayerController.instance.useGun*/ PlayerPrefs.HasKey("useGun"))
        {
            Shoot(direction);
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            ReloadGun();
        }

        GunFlipController(mousePos);
    }

    private void GunFlipController(Vector3 mousePos)
    {


        if (mousePos.x < gun.position.x && gunFacing)
        {
            GunFlip();
        }
        else if (mousePos.x > gun.position.x && !gunFacing)
        {
            GunFlip();
        }
    }

    private void GunFlip()
    {
        gunFacing = !gunFacing;
        gun.localScale = new Vector3(gun.localScale.x, gun.localScale.y * -1, gun.localScale.z);
    }

    public void Shoot(Vector3 direction)
    {
        UIController.instance.UpdateBulletCount(currentBullet, maxBullet);

        GameObject newBullet = Instantiate(bulletPrefab, gun.position, Quaternion.identity);
        newBullet.GetComponent<Rigidbody2D>().velocity = direction.normalized * bullerSpeed;

        Destroy(newBullet, 7);
    }

    public void ReloadGun()
    {
        currentBullet = maxBullet;
        UIController.instance.UpdateBulletCount(currentBullet, maxBullet);
    }

    public bool HaveBullet()
    {
        if (currentBullet <= 0)
        {
            return false;
        }

        currentBullet--;
        return true;
    }
}
