﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Victory : MonoBehaviour
{
    public string mainMenu;

    public AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        string musicStatus = PlayerPrefs.GetString("musicStatus", "true");
        bool isMusicOn = bool.Parse(musicStatus);

        if (isMusicOn)
        {
            audioSource.Play();
        }
        else
        {
            audioSource.Stop();
        }
    }

    

    public void MainMenu()
    {
        SceneManager.LoadScene(mainMenu);
    }
}
