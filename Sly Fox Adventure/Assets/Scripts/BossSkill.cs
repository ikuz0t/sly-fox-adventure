using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossSkill : MonoBehaviour
{
    public GameObject spawn1;
    public GameObject spawn2;
    public GameObject spawn3;


    // Update is called once per frame
    void Update()
    {
        if (BossTankController.instance.health == 3)
        {
            spawn1.SetActive(true);
            spawn2.SetActive(true);
            spawn3.SetActive(true);
        }
        else if (BossTankController.instance.health == 1)
        {
            spawn1.SetActive(false);
            spawn2.SetActive(false);
            spawn3.SetActive(false);
        }
    }
}
