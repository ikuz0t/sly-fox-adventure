﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AudioManager : MonoBehaviour
{
	public static AudioManager instance;

	public AudioSource[] soundEffects;

	public AudioSource bgm, levelEndMusic, bossMusic;

	private void Awake()
	{
		instance = this;
	}

	// Start is called before the first frame update
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{

	}

	public void PlaySFX(int soundToPlay)
	{
        string soundStatus = PlayerPrefs.GetString("soundStatus", "true");
        bool isSoundOn = bool.Parse(soundStatus);

        if (isSoundOn)
        {
            soundEffects[soundToPlay].Stop();

            soundEffects[soundToPlay].pitch = Random.Range(.9f, 1.1f);

            soundEffects[soundToPlay].Play();
        }
	}

	public void PlayLevelVictory()
	{
        string musicStatus = PlayerPrefs.GetString("musicStatus", "true");
        bool isMusicOn = bool.Parse(musicStatus);

        if (isMusicOn)
        {
            bgm.Stop();
            levelEndMusic.Play();
        }
	}

	public void PlayBossMusic()
	{
        string musicStatus = PlayerPrefs.GetString("musicStatus", "true");
        bool isMusicOn = bool.Parse(musicStatus);

        if (isMusicOn)
        {
		    bgm.Stop();
			bossMusic.Play();
        }
	}

	public void StopBossMusic()
	{
        string musicStatus = PlayerPrefs.GetString("musicStatus", "true");
        bool isMusicOn = bool.Parse(musicStatus);

        if (isMusicOn)
        {
            bossMusic.Stop();
            bgm.Play();
        }    
	}



}
