using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingMusic : MonoBehaviour
{
	public GameObject settingSFX;

	// Start is called before the first frame update
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{

	}

	public void OpenSettingSFX()
	{
		settingSFX.SetActive(true);
	}
	public void CloseSettingSFX()
	{
		settingSFX.SetActive(false);
	}
}
