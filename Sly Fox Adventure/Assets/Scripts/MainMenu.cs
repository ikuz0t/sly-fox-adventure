﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
	public static MainMenu instance;

	public string startScene, continueScene;

	public AudioSource audioSource;

	public GameObject continueButton;

	public GameObject settingSFX;

	public GameObject iconSoundOn;
	public GameObject iconSoundOff;

    public GameObject iconMusicOn;
    public GameObject iconMusicOff;

    public bool soundOn;
    public bool musicOn;

    private void Awake()
    {
        instance = this;
    }

    private void OnEnable()
    {
        string soundStatus = PlayerPrefs.GetString("soundStatus", "true");
        string musicStatus = PlayerPrefs.GetString("musicStatus", "true");

        bool isSoundOn = bool.Parse(soundStatus);
        bool isMusicOn = bool.Parse(musicStatus);

        if (isSoundOn)
        {
            OpenSound();
        }
        else
        {
            OffSound();
        }

        if (isMusicOn)
        {
            OpenMusic();
        }
        else
        {
            OffMusic();
        }
    }

    public void OpenSound()
	{
        iconSoundOn.SetActive(true);
        iconSoundOff.SetActive(false);

        PlayerPrefs.SetString("soundStatus", "true");
    }

	public void OffSound()
	{
        iconSoundOn.SetActive(false);
		iconSoundOff.SetActive(true);

        PlayerPrefs.SetString("soundStatus", "false");
    }

    public void OpenMusic()
    {
        iconMusicOn.SetActive(true);
        iconMusicOff.SetActive(false);

        PlayerPrefs.SetString("musicStatus", "true");

		audioSource.Play();
    }

    public void OffMusic()
    {
        iconMusicOn.SetActive(false);
        iconMusicOff.SetActive(true);

        PlayerPrefs.SetString("musicStatus", "false");

		audioSource.Stop();
    }


    // Start is called before the first frame update

    public void OpenSettingSFX()
	{
		settingSFX.SetActive(true);
	}

	public void CloseSettingSFX()
	{
		settingSFX.SetActive(false);
	}

	// Start is called before the first frame update
	void Start()
	{
		if (PlayerPrefs.HasKey(startScene + "_unlocked"))
		{
			continueButton.SetActive(true);
		}
		else
		{
			continueButton.SetActive(false);
		}
	}

	// Update is called once per frame

	public void StartGame()
	{
		SceneManager.LoadScene(startScene);

		PlayerPrefs.DeleteAll();
	}

	public void ContinueGame()
	{
		SceneManager.LoadScene(continueScene);
	}

	public void QuitGame()
	{
		Application.Quit();
		Debug.Log("Quitting Game");
	}
}
